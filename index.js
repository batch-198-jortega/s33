// express package was imported as express.
const express = require("express");

// invoked express package to create a server/api and saved it in a veriable which we can refer to later to create routes.
const app = express();

// express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request.

// app.ust() is a method used to run another function or method for our expressjs API.
// It is used to run middlewares (functions that add features to our application).
app.use(express.json());

// variable for port assignment
const port = 4000;

/*Mock Collection for Courses*/
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];

let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}
	
];

// used the listen() method of express to assign a port to our server and send a message

// creating a route in Express:
/*
	Syntax:
								 *arrow function method*
	app.requestMethod('/endpoint',(req,res) => {

		- send() is a method similar to end() that is sends the data/message and ends the response.
		- It also automatically creates and adds the headers.

		response.send();

	});
*/
// access express to have access to its route methods.


app.get('/',(req,res) => {

	res.send("Hello from our first ExpressJS route!");

});

app.post('/',(req,res) => {

	res.send("Hello from our first ExpressJS Post Method Route!");

});

/*
	Mini-Activity

	Create a route for a put method request on the "/" endpoint.
	Send a message as a response: "Hello from a put method route!"

	Create a route for a delete method request on the "/" endpoint.
	Send a message as a response: "Hello from a delete method route!"
*/

app.put('/',(req,res) => {

	res.send("Hello from a put method route!");

});

app.delete('/',(req,res) => {

	res.send("Hello from a delete method route!");

});

app.get('/courses',(req,res)=>{

	res.send(courses);

});

// Create a route to be able to add a new course from an input from a request.

app.post('/courses',(req,res) => {

	// with express.json() the data stream has been captured, the data input has been pased into a JS Object.

	// Note: Every time you need to access or use the request body, log it in the console first.
	// console.log(req.body); // object
	//request or req.body contains the body of the request or the input passed.

	// Add the input as req.body into our courses array
	courses.push(req.body);

	// console.log(courses); Check if push was successful

	// Send the updated users array in the client
	res.send(courses);

});

/*
	Activity:

	Same Endpoint: /users

	Create a new route to get and send the users array in the client.
		-Check the get method route for our courses for reference.

	Create a new route to create and add a new user object in the users array.
		-Send the updated users array in the client.
		-Check the post method route for our courses for reference.

	Stretch Goal

	Create a new route which can delete the last item in the users array.
		-Send a simple message that a user has been deleted successfully.

	Stretch Goal 2

	Create a new route which can update the price of a single item in the array.
		-Pass the index number of the item you want to update in the request body.
		-Add the index number of the item you want to update to access it and its price property.
		-Re-assign the new price from our request body.
		-Send the updated item in the client.
*/

app.get('/users',(req,res) => {

	res.send(users);

});

app.post('/users',(req,res) => {

	// console.log(req.body); // check if data is being received
	users.push(req.body);
	// console.log(users); // check if push is successful
	res.send(users);

});

app.delete('/users',(req,res) => {

	users.pop();
	res.send("User has been deleted successfully.");

});

app.patch('/courses',(req,res) => {

	// console.log(req.body);
	// console.log(req.body.index);
	let index = parseInt(req.body.index);
	let key = Object.keys(req.body)[1];

	Object.keys(req.body).forEach((k) => {

		if (k in courses[index]) {

			courses[index][k] = req.body[k];
		};
	});

	res.send(courses[index]);
	
/*	if(key === "name") {

		courses[index].name = req.body.name;
		res.send(courses[index]);

	} else if (key === "description") {

		courses[index].description = req.body.description;
		res.send(courses[index]);

	} else if (key === "price") {

		courses[index].price = req.body.price;
		res.send(courses[index]);

	};*/

});

app.listen(port,() => console.log(`Express API running at port 4000`));